==============================================================
Run the UVM Express examples, either with 'make' or a do-file.
==============================================================

Running with a do-file:

	cd examples/<example-directory>
	vsim -do run.do

	For example:

	    cd examples/01-simple-bfm-interface/  
	    vsim -do run.do
    
	    cd examples/02-functional-coverage/  
	    vsim -do run.do
    
	    cd examples/03-random-stimulus/
	    vsim -do run.do
    

Running with 'make':

	cd examples/<example-directory>
	make

	For example:

	    cd examples/01-simple-bfm-interface/  
	    make 
    
	    cd examples/02-functional-coverage/  
	    make
    
	    cd examples/03-random-stimulus/
	    make
    


To run in GUI mode, after having compiled everything using 'make' or 'run.do':

    vsim tests top


To run with greater debug visibility (Questa 10.1):

	vsim -uvmcontrol=all -classdebug -msgmode both tests top


Other views (Memory window and Waveform)
  In each example directory there are two additional do files for
  a more interesting debug - mem.do and wave.do
  
  To use these files, start simulation, then run 0, and source the do files:

    VSIM> run 0;
    VSIM> source mem.do
    VSIM> source wave.do
    VSIM> run -all

