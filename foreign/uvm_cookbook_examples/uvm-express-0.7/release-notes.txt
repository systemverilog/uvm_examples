General Overview

--------------------------------------------------------------
Release Notes for UVM Express 0.7
Feb 17, 2012

** Release Overview
  - Minor fixes
    + Imports moved inside module.
    + Renamed axx_test to axx_env

  - Added enum in the transaction definition for better
    transaction debug display. Deleted transaction naming
    that implmented similar functionality.

  - 03 tests now issues 1000 transactions instead of 100; improves coverage.
  - Simplified abc_transaction
  - Cleaner objection handling for functional coverage example.

--------------------------------------------------------------
Release Notes for UVM Express 0.6
Feb 8, 2012

** Release Overview

  - 'do-file' runnable (in addition to makefile)

  - Much simpler functional coverage 'coordination' implementation.
  - Better covergroups.

  - Enhanced makefiles with consistent debug switches.
    VSIM_OPTIONS ?= -cvg63 -cover -uvmcontrol=all -classdebug -msgmode both

  - Replaced set_config_int with uvm_config_db. (What's better than a double
    double colon line?)

    <     set_config_int("*", "recording_detail", 1);
    ---
    >     uvm_config_db#(int)::set(uvm_root::get(), "*", "recording_detail", 1);

  - $psprintf --> $sformatf

--------------------------------------------------------------
Release Notes for UVM Express 0.5
Jan 3, 2012

** Release Overview

  - Makefile cleanup
  - Bug fixes.
    + BFM functionality
    + Sequence randomize constraint fixed/made clearer
  - Added details transaction recording in the BFM.
  - DUT sleep mode working
  - DUT atomic access to the memory working

--------------------------------------------------------------
Release Notes for UVM Express 0.4
Oct 31, 2011

** Release Overview

  - Fourth release.

    + abc_pkg agent split out
      abc_pkg has coverage, sequences, etc. It is a "full agent".
      abc_coverage_pkg has just coverage and transactions. It is
        a "coverage-only agent". There is duplicated code between
        the two.
      is_active is_gone.

    + Ending considerations
      o run_test() starts the test - 
        ready_to_finish() stops the test

    + Simplified structure
      o Simpler.
        Test 01-simple-bfm-interface is yet simpler. 
        
      o Test/Top
        All examples are "top" --> top.sv and 
                       "tests" --> tests.sv.
         1) Tests are in tests.sv.
         2) top.sv doesn't need to `include the abc_if.svh
        Very simple top.sv.
        Very simple tests.sv. Use "top.bfm1"!

      o Monitor
        Monitor implementation simplified and moved 
          into the BFM.

      o BFM/VIF Hookup
        Replaced:
          uvm_config_db#(virtual abc_if)::set(
            uvm_root::get(), "", "bfm", bfm);
          run_test("abc_agent");
    
        With:
          abc_test::register("bfm1", top.bfm1);
          abc_test::register("bfm2", top.bfm2);
          abc_test::register("bfm3", top.bfm3);
          run_test("abc_test");

      o Agent vs Agent+Test
        An agent expects to be instantiated inside a "test". 
          A test is used to configure or control the agent.

    + Testing
      More complete test.
        Turned on all three BFMs in example 02.

    
    + Oddities
      o "coverage_ok" hackery removed.
      o Added mem.do

** API Changes

** Deprecated APIs

** Bug Fixes

** Documentation Fixes

--------------------------------------------------------------
Release Notes for UVM Express 0.3
Oct 26, 2011

** Release Overview

  - Third release.
    DUT is connected by signals, not interfaces.
    DUT now has three interfaces (3 bundles of signals)
    No more directed tests in example 03.
    Split DATA into DATAI and DATAO.
    Burst! Now.
    DUT has "special memory" address ranges now. 
      For cool stuff later.
    Implemented BFM/DUT simple "debug dump()" messaging.

** API Changes

** Deprecated APIs

** Bug Fixes

** Documentation Fixes

--------------------------------------------------------------
Release Notes for UVM Express 0.2
Oct 21, 2011

** Release Overview

  - Second release.
    clk is now in the BFM portlist.
    tests are now tasks in the top module.
    import uvm_pkg and use `include uvm_macros.svh --> 
      good practice
    removed excess ::
    coverage has sample(t) now.
    env is now agent.
    burst implemented.
    fixed some weird interface coding issues 
     (READY before VALID)
    reset transaction type implemented


** API Changes

** Deprecated APIs

** Bug Fixes

** Documentation Fixes

--------------------------------------------------------------
Release Notes for UVM Express 0.1
Oct 19, 2011

** Release Overview

  - First release.

** API Changes

** Deprecated APIs

** Bug Fixes

** Documentation Fixes


