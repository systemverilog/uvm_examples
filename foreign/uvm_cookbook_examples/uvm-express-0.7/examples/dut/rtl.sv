//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

module dut_mem(
  input  wire        CLK1, 
  input  wire        RST1, 
  input  wire        RW1, 
  output reg         READY1, 
  input  wire        VALID1, 
  input  reg  [31:0] ADDR1,
  input  reg  [31:0] DATAI1,
  output reg  [31:0] DATAO1,
  
  input  wire        CLK2, 
  input  wire        RST2, 
  input  wire        RW2, 
  output reg         READY2, 
  input  wire        VALID2, 
  input  reg  [31:0] ADDR2,
  input  reg  [31:0] DATAI2,
  output reg  [31:0] DATAO2,
  
  input  wire        CLK3, 
  input  wire        RST3, 
  input  wire        RW3, 
  output reg         READY3, 
  input  wire        VALID3, 
  input  reg  [31:0] ADDR3,
  input  reg  [31:0] DATAI3,
  output reg  [31:0] DATAO3
 );

  
  // Address map
  // Interface1&2 :   0- 31 [0x00-0x1F) (32 addresses)
  // Shared(1,2,3): 32 - 95 [0x20-0x5F) (64 addresses)
  // Interface1&3 : 96 -127 [0x60-0x7F) (32 addresses)
  reg[31:0] mem[128];

  reg save_ready_1, save_ready_2, save_ready_3;
  bit verbose_sleep = 0;
  bit power_save_enabled = 0;

  function automatic void dump();
    for(int i = 0; i < 128; i+=8) begin
      $write("[%03x] ", i);
      for(int j = i; j < i+8; j++)
        if (j < 128) $write("%03x,", mem[j]);
      if (i >=32 && i < 96)
        $write(" *");
      $write("\n");
    end
  endfunction

  task automatic mem_reset(ref reg READY);
    READY = 0;
    foreach (mem[i])
      #1 mem[i] = 'x;
    READY = 1;
  endtask

  // Every now and then the DUT goes to sleep, and then
  // spontaneously wakes up 20 clocks later.
  always begin
    repeat(100)
      @(posedge CLK1);

    if (power_save_enabled) begin
      save_ready_1 = READY1;
      save_ready_2 = READY2;
      save_ready_3 = READY3;
      if (verbose_sleep) $display("@ %0t: DUT going to sleep...(%0b, %0b, %0b)", $time, READY1, READY2, READY3);
      READY1 = 0; // Go to sleep.
      READY2 = 0; // Go to sleep.
      READY3 = 0; // Go to sleep.
      repeat(20)
        @(posedge CLK1);
      if (verbose_sleep) $display("@ %0t: DUT waking up ...(%0b, %0b, %0b)", $time, READY1, READY2, READY3);
      wait(RST1 == 0); // Can't wake up until after reset.
      READY1 = READY1|save_ready_1; // Go back to where we were.
      READY2 = READY2|save_ready_2; // Go back to where we were.
      READY3 = READY3|save_ready_3; // Go back to where we were.
      if (verbose_sleep) $display("@ %0t: DUT ready ...(%0b, %0b, %0b)", $time, READY1, READY2, READY3);
    end
  end

  semaphore shared_access;
  bit verbose_lock = 0;

  static bit m_initialized = m_init();
  function bit m_init();
    shared_access = new(1);
    return 1;
  endfunction

  task automatic m_lock(int interface_number, input bit[31:0]addr);
    if (addr >= 128) return;
    if ((addr >= 'h20) && (addr < 'h60)) begin
      if (verbose_lock) $display("@ %5t: DUT[%0d] - ADDR (%0x)   LOCKED", 
        $time, interface_number, addr);
      shared_access.get();
    end
  endtask

  task automatic m_unlock(int interface_number, bit[31:0]addr);
    if (addr >= 128) return;
    if ((addr >= 'h20) && (addr < 'h60)) begin
      shared_access.put();
      if (verbose_lock) $display("@ %5t: DUT[%0d] - ADDR (%0x) UNLOCKED", 
        $time, interface_number, addr);
    end
  endtask

  task automatic mem_access(
    input int interface_number,
    input bit CLK,
    ref reg READY,
    input bit RW, 
    input bit[31:0]ADDR, 
    input bit[31:0]DATAI, 
    output bit[31:0]DATAO);

    if (ADDR == 'hffffffff) begin
      dump();
      return;
    end

    if (interface_number == 1)
      if (!(ADDR >= 0) && (ADDR < 'h60)) begin
        $display("1@ %5t: DUT[%0d] - ADDR RANGE ERROR @ %0x. Legal range [%0x:%0x]", 
          $time, interface_number, ADDR, 'h00, 'h60);
        return;
      end

    if (interface_number == 2)
      if (!((ADDR >= 'h20) && (ADDR < 'h80))) begin
        $display("2@ %5t: DUT[%0d] - ADDR RANGE ERROR @ %0x. Legal range [%0x:%0x]", 
          $time, interface_number, ADDR, 'h20, 'h80);
        return;
      end

    if (ADDR > 127)
      $display("@ %5t: DUT[%0d] - BUS ERROR @ %0x", 
        $time, interface_number, ADDR);

    if (RW == 1) begin      // READ
      READY = 0;
      m_lock(interface_number, ADDR);
      repeat (3) @(posedge CLK1); // Slow read.
      DATAO = mem[ADDR];
      m_unlock(interface_number, ADDR);
      READY = 1;
      $display("@ %5t: DUT[%0d] - READ (%0x, %0x)", 
        $time, interface_number, ADDR, DATAO);
    end
 
    else if (RW == 0) begin // WRITE
      DATAO = 'z;
      mem[ADDR] = DATAI;
      $display("@ %5t: DUT[%0d] - WRITE(%0x, %0x)", 
        $time, interface_number, ADDR, DATAI);
    end

    else begin                  // IDLE
      $display("@ %5t: DUT[%0d] - IDLE - RW=%0b", 
        $time, interface_number, RW);
      assert(0);
    end
  endtask

  always @(posedge CLK1) begin
    if (RST1 == 1) begin          // RESET
      mem_reset(READY1);
    end
    else if ((READY1 == 1) && 
             (VALID1 == 1)) begin // READ/WRITE
      mem_access(1, CLK1, READY1, RW1, ADDR1, DATAI1, DATAO1);
    end
  end

  always @(posedge CLK2) begin
    if (RST2 == 1) begin          // RESET
      mem_reset(READY2);
    end
    else if ((READY2 == 1) && 
             (VALID2 == 1)) begin // READ/WRITE
      mem_access(2, CLK2, READY2, RW2, ADDR2, DATAI2, DATAO2);
    end
  end

  always @(posedge CLK3) begin
    if (RST3 == 1) begin          // RESET
      mem_reset(READY3);
    end
    else if ((READY3 == 1) && 
             (VALID3 == 1)) begin // READ/WRITE
      mem_access(3, CLK3, READY3, RW3, ADDR3, DATAI3, DATAO3);
    end
  end
endmodule
