//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class axx_coverage_test extends uvm_component;
  `uvm_component_utils(axx_coverage_test)

  // Set from configurations
  virtual abc_if bfm1;
  virtual xyz_if bfm2;
  virtual xyz_if bfm3;

  abc_coverage_agent agent1;
  xyz_coverage_agent agent2;
  xyz_coverage_agent agent3;

  function new(string name = "axx_coverage_test", 
      uvm_component parent = null);
    super.new(name, parent);
  endfunction

  static function void register_abc(
    string name, virtual abc_if vif);

    uvm_config_db#(virtual abc_if)::
      set(uvm_root::get(), "", name, vif);
  endfunction

  static function void register_xyz(
    string name, virtual xyz_if vif);

    uvm_config_db#(virtual xyz_if)::
      set(uvm_root::get(), "", name, vif);
  endfunction

  function void build_phase(uvm_phase phase);
    if( !uvm_config_db#(virtual abc_if)::
        get(this, "", "bfm1", bfm1))
      `uvm_fatal("ENV", "BFM1 not set")

    if( !uvm_config_db#(virtual xyz_if)::
        get(this, "", "bfm2", bfm2))
      `uvm_fatal("ENV", "BFM2 not set")

    if( !uvm_config_db#(virtual xyz_if)::
        get(this, "", "bfm3", bfm3))
      `uvm_fatal("ENV", "BFM3 not set")

    uvm_config_db#(virtual abc_if)::
      set( this, "agent1", "bfm", bfm1);
    uvm_config_db#(virtual xyz_if)::
      set( this, "agent2", "bfm", bfm2);
    uvm_config_db#(virtual xyz_if)::
      set( this, "agent3", "bfm", bfm3);

    agent1 = abc_coverage_agent::type_id::
      create("agent1", this);
    agent2 = xyz_coverage_agent::type_id::
      create("agent2", this);
    agent3 = xyz_coverage_agent::type_id::
      create("agent3", this);
  endfunction

  static uvm_phase         m_phase    [string];
  static axx_coverage_test m_component[string];

  task run_phase(uvm_phase phase);
    `uvm_info("RUN", $sformatf("Starting up '%s'", 
      get_full_name()), UVM_MEDIUM)
    m_phase    [get_full_name()] = phase;
    m_component[get_full_name()] = this;

    phase.raise_objection(this);
  endtask

  static task ready_to_finish();
    abc_coverage_agent::ready_to_finish();
    xyz_coverage_agent::ready_to_finish();

    foreach(m_component[s]) begin
      m_component[s].uvm_report_info("RUN", 
        $sformatf("Shutting down '%s'", s), UVM_MEDIUM);
      m_phase[s].drop_objection(m_component[s]);
    end
  endtask
endclass
