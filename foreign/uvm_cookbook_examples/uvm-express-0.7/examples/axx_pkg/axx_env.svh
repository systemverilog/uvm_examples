//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class axx_env extends uvm_env;
  `uvm_component_utils(axx_env)

  // Set from configurations
  virtual abc_if bfm1;
  virtual xyz_if bfm2;
  virtual xyz_if bfm3;

  abc_agent agent1;
  xyz_agent agent2;
  xyz_agent agent3;

  function new(string name = "axx_env", 
      uvm_component parent = null);
    super.new(name, parent);
  endfunction

  static function void register_abc(
    string name, virtual abc_if vif);

    uvm_config_db#(virtual abc_if)::
      set( uvm_root::get(), "", name, vif);
  endfunction

  static function void register_xyz( 
    string name, virtual xyz_if vif);

    uvm_config_db#(virtual xyz_if)::
      set( uvm_root::get(), "", name, vif);
  endfunction

  function void build_phase(uvm_phase phase);
    if( !uvm_config_db#(virtual abc_if)::
        get( this, "", "bfm1", bfm1)) 
      `uvm_fatal("ENV", "BFM1 not set")

    if( !uvm_config_db#(virtual xyz_if)::
        get( this, "", "bfm2", bfm2))
      `uvm_fatal("ENV", "BFM2 not set")

    if( !uvm_config_db#(virtual xyz_if)::
        get( this, "", "bfm3", bfm3))
      `uvm_fatal("ENV", "BFM3 not set")

    uvm_config_db#(virtual abc_if)::
      set( this, "agent1", "bfm", bfm1);
    uvm_config_db#(virtual xyz_if)::
      set( this, "agent2", "bfm", bfm2);
    uvm_config_db#(virtual xyz_if)::
      set( this, "agent3", "bfm", bfm3);

    agent1 = abc_agent::type_id::create("agent1", this);
    agent2 = xyz_agent::type_id::create("agent2", this);
    agent3 = xyz_agent::type_id::create("agent3", this);
  endfunction

  task run_phase(uvm_phase phase);
    abc_even_sequence seq1;
    xyz_sequence seq2, seq3;

    `uvm_info("RUN", $sformatf("Starting up '%s'", 
      get_full_name()), UVM_MEDIUM)

    seq1 = abc_even_sequence::type_id::create("seq1");
    seq2 = xyz_sequence::type_id::create("seq2");
    seq3 = xyz_sequence::type_id::create("seq3");
    
    seq1.addr_low = 'h00; seq1.addr_high = 'h60;
    seq1.number_of_items = 1000;

    seq2.addr_low = 'h20; seq2.addr_high = 'h80;
    seq2.number_of_items = 1000;

    seq3.addr_low = 'h00; seq3.addr_high = 'h80;
    seq3.number_of_items = 1000;
    
    phase.raise_objection(this);
    fork
      seq1.start(agent1.sequencer);
      seq2.start(agent2.sequencer);
      seq3.start(agent3.sequencer);
    join
    phase.drop_objection(this);
    $display("UVM TEST PASSED");
  endtask
endclass

