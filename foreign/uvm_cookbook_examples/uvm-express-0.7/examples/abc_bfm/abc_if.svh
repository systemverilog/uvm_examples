//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

interface automatic abc_if(input wire CLK);
  // TODO: modports?

  // -------------------------------------------
  // The signals.
  logic RST;

  logic VALID; // Goes high when the addr/data is valid.
  logic READY; // Goes high when the DUT is ready.

  logic RW;    // READ = 1, WRITE = 0
  logic BURST; // High when a burst begins, low when it ends.

  logic [31:0] ADDR;

  logic [31:0] DATAI;
  wire[31:0] DATAO;

  int stream_h;
  int monitor_stream_h;

  // -------------------------------------------
  // Placeholder for checkers/assertions/...

  bit[2:0] fire;
  ovl_handshake handshake(
    .clock(CLK), 
    .reset(RST), 
    .enable('1), 
    .req(VALID), 
    .ack(READY), 
    .fire(fire));

  // -------------------------------------------
  // Debug
  task dump();
    write('1, '1);
  endtask

  function void check_stream(string stream_name = "test");
    if (stream_h == 0) begin
      stream_h =         $create_transaction_stream( 
        {"..", stream_name},         "kind");
      monitor_stream_h = $create_transaction_stream( 
        {"..", stream_name, "_mon"}, "kind");
    end

  endfunction

  // -------------------------------------------
  // The tasks to interact with the signals.

  task automatic reset();
    int tr;
    check_stream();
    tr = $begin_transaction(stream_h, "RESET");
    $display("@ %05t: %m RESET", $time);
    RST <= 1;
    @(posedge CLK);
    RST <= 0;
    @(posedge READY);
    $display("@ %05t: %m RESET DONE", $time);
    $end_transaction(tr);
    $free_transaction(tr);
  endtask

  task automatic read(bit[31:0] addr, output bit[31:0] data);
    int tr;
    check_stream();
    tr = $begin_transaction(stream_h, "read");
    $add_attribute(tr, addr, "addr");
    RW <= 1;       // Going to be a READ.
    BURST <= 0;    // Not a burst.
    ADDR <= addr;  // Put out the address.
    VALID <= 1;    // Signal valid...
    DATAI <= 'z;
    @(negedge CLK);
    while (!READY) wait(READY); // Wait for response ready.
    @(posedge CLK);

    @(negedge CLK);
    while (!READY) wait(READY); // Wait for response ready.
    @(posedge CLK);
    $display("@ %05t: %m READ (%0x)", $time, addr);
    data = DATAO;  // Get the response
    $add_attribute(tr, data, "rdata");
    $display("@      : %m READ (%0x)", data);
    VALID <= 0;    // Not valid any more.
    $end_transaction(tr);
    $free_transaction(tr);
  endtask

  task automatic write(bit[31:0] addr, input bit[31:0] data);
    int tr;
    check_stream();
    tr = $begin_transaction(stream_h, "write");
    $add_attribute(tr, addr, "addr");
    $add_attribute(tr, data, "wdata");
    RW <= 0;
    BURST <= 0;
    ADDR <= addr;
    DATAI <= data;
    VALID <= 1;
    @(negedge CLK);
    while (!READY) wait(READY);
    @(posedge CLK);
    $display("@ %05t: %m WRITE(%0x, %0x)", $time, addr, data);
    VALID <= 0;
    $end_transaction(tr);
    $free_transaction(tr);
  endtask

  task automatic burst_read(bit[31:0] addr, output bit[31:0] data[4]);
    int tr_parent;
    int tr_phase;
    check_stream();
    tr_parent = $begin_transaction(stream_h, "burst_read");
    $add_attribute(tr_parent, addr, "addr");
    $add_attribute(tr_parent, 4,    "length");
    RW <= 1;     // Going to be a READ.
    BURST <= 1;  // Burst yes.
    DATAI <= 'z;
    for(int i = 0; i < 4; i++) begin
      tr_phase = $begin_transaction( stream_h, 
        $sformatf("phase_%0d", i), $time, tr_parent);
      $add_relation(tr_parent, tr_phase,  "parent");
      $add_relation(tr_phase , tr_parent, "child");
      ADDR <= addr+i; // Update the burst address.
      $add_attribute(tr_phase, addr+i, "ADDR");
      VALID <= 1;     // Signal a valid transfer.
      @(negedge CLK);
      while (!READY) wait(READY);
      @(posedge CLK);

      @(negedge CLK);
      while (!READY) wait(READY);
      @(posedge CLK);
      data[i] = DATAO;// Get the response.
      $add_attribute(tr_phase, data[i], "DATA");
      $end_transaction(tr_phase);
      $free_transaction(tr_phase);
    end
    VALID <= 0;  // All done.
    $add_attribute(tr_parent, data, "burst_data");
    $end_transaction(tr_parent);
    $free_transaction(tr_parent);
    $display("@ %05t: %m BURST_READ (%0x, %0x, %0x, %0x, %0x)", 
      $time, addr, data[0], data[1], data[2], data[3]);
  endtask

  task automatic burst_write(bit[31:0] addr, bit[31:0]data[4]);
    int tr_parent;
    int tr_phase;
    check_stream();
    tr_parent = $begin_transaction(stream_h, "burst_write");
    $add_attribute(tr_parent, addr, "addr");
    $add_attribute(tr_parent, data, "burst_data");
    $add_attribute(tr_parent, 4,    "length");
    RW <= 0;
    BURST <= 1;
    foreach (data[i]) begin
      tr_phase = $begin_transaction(
        stream_h, 
        $sformatf("phase_%0d", i), 
        $time, 
        tr_parent);
      $add_relation(tr_parent, tr_phase,  "parent");
      $add_relation(tr_phase , tr_parent, "child");
      ADDR <= addr+i;
      DATAI <= data[i];
      $add_attribute(tr_phase, addr+i, "ADDR");
      $add_attribute(tr_phase, data[i], "DATA");
      VALID <= 1;
      @(negedge CLK);
      while (!READY) wait(READY);
      @(posedge CLK);
      if (i == 0)
        $display("@ %05t: %m BURST_WRITE(%0x, %0x, %0x, %0x, %0x)", 
          $time, addr, data[0], data[1], data[2], data[3]);
      $end_transaction(tr_phase);
      $free_transaction(tr_phase);
    end
    VALID <= 0;
    $end_transaction(tr_parent);
    $free_transaction(tr_parent);
  endtask

  function automatic string get_tr_name(bit burst, bit rw);
    string tr_name;
         if ((burst==1) && (rw==1)) tr_name = "BURST_READ";
    else if ((burst==1) && (rw==0)) tr_name = "BURST_WRITE";
    else if ((burst==0) && (rw==1)) tr_name = "READ";
    else if ((burst==0) && (rw==0)) tr_name = "WRITE";
    return tr_name;
  endfunction

  task automatic monitor(output bit transaction_ok, 
                      bit rw, 
                      bit[31:0]addr, 
                      bit[31:0]data[4], 
                      bit burst);

    int tr;

    check_stream();
    transaction_ok = 0;
    @(negedge CLK);
    if ((READY == '1) && (VALID == '1) && (BURST == '1)) begin 
      int tr_phase;
      int burst_count;

      burst = 1;
      rw = RW;
      addr = ADDR;
      tr = $begin_transaction(monitor_stream_h, 
        $sformatf("MON_%s", get_tr_name(burst, rw)));
      $add_attribute(tr, addr, "addr");

      tr_phase = 0;
      burst_count = 0;
      //while ((READY == '1) && (VALID == '1) && (BURST == '1)) begin 
      while (1) begin
        if (tr_phase != 0) begin
          $end_transaction(tr_phase);
          $free_transaction(tr_phase);
        end
        tr_phase = $begin_transaction(
          monitor_stream_h, $sformatf("phase_%0d", burst_count), $time, tr);
        $add_attribute(tr_phase, addr, "ADDR");
        if (rw == 1) begin // READ
          while (!READY) wait(READY);
          @(posedge CLK);
          data[burst_count] = DATAO;
          @(negedge CLK);
        end
        else begin        // WRITE
          while (!READY) wait(READY);
          @(posedge CLK);
          data[burst_count] = DATAI;
          @(negedge CLK);
        end
        $add_attribute(tr_phase, data[burst_count], "DATA");
        burst_count++;
        if (burst_count >= 4) 
          break;
      end
      if (burst_count != 4) 
        $display( "(%m) MONITOR ERROR: [BURST %s] Burst error - %0d should have been 4", 
          (RW==1)?"READ":"WRITE", burst_count);

      $add_attribute(tr, data, "data");
      $end_transaction(tr);
      $free_transaction(tr);
      transaction_ok = 1;
    end
    else if ((READY == '1) && (VALID == '1) && (BURST == '0)) begin
      burst = 0;
      rw = RW;
      addr = ADDR;
      tr = $begin_transaction(monitor_stream_h, 
        $sformatf("MON_%s", get_tr_name(burst, rw)));
      $add_attribute(tr, addr, "addr");
      @(posedge CLK);
      if (rw == 1) // READ
        data[0] = DATAO;
      else         // WRITE
        data[0] = DATAI;
      $add_attribute(tr, data, "data");
      $end_transaction(tr);
      $free_transaction(tr);
      transaction_ok = 1;
    end
  endtask
endinterface
