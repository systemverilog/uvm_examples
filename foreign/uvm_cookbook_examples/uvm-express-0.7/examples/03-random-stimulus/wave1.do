onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -childformat {{/uvm_root/uvm_test_top/agent1/driver/stream.s0.addr -radix decimal} {/uvm_root/uvm_test_top/agent1/driver/stream.s0.data -radix decimal}} -expand -subitemconfig {/uvm_root/uvm_test_top/agent1/driver/stream.s0.addr {-radix decimal} /uvm_root/uvm_test_top/agent1/driver/stream.s0.data {-radix decimal} /uvm_root/uvm_test_top/agent1/driver/stream.s0.s0 {-childformat {{/uvm_root/uvm_test_top/agent1/driver/stream.s0.s0.ADDR -radix decimal} {/uvm_root/uvm_test_top/agent1/driver/stream.s0.s0.DATA -radix decimal}} -expand} /uvm_root/uvm_test_top/agent1/driver/stream.s0.s0.ADDR {-radix decimal} /uvm_root/uvm_test_top/agent1/driver/stream.s0.s0.DATA {-radix decimal}} /uvm_root/uvm_test_top/agent1/driver/stream
add wave -noupdate -childformat {{/uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.data -radix decimal} {/uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.addr -radix decimal}} -expand -subitemconfig {/uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.data {-radix decimal} /uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.addr {-radix decimal} /uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.m0 {-childformat {{/uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.m0.ADDR -radix decimal} {/uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.m0.DATA -radix decimal} {/uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.m0.addr -radix decimal}} -expand} /uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.m0.ADDR {-radix decimal} /uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.m0.DATA {-radix decimal} /uvm_root/uvm_test_top/agent1/monitor/monitor_stream.m0.m0.addr {-radix decimal}} /uvm_root/uvm_test_top/agent1/monitor/monitor_stream
add wave -noupdate -divider {BFM 1}
add wave -noupdate /top/bfm1/CLK
add wave -noupdate /top/bfm1/RST
add wave -noupdate /top/bfm1/VALID
add wave -noupdate /top/bfm1/READY
add wave -noupdate /top/bfm1/RW
add wave -noupdate /top/bfm1/BURST
add wave -noupdate -radix decimal /top/bfm1/ADDR
add wave -noupdate -radix decimal /top/bfm1/DATAI
add wave -noupdate -radix decimal /top/bfm1/DATAO
add wave -noupdate -divider {BFM 2}
add wave -noupdate /top/bfm2/CLK
add wave -noupdate /top/bfm2/RST
add wave -noupdate /top/bfm2/VALID
add wave -noupdate /top/bfm2/READY
add wave -noupdate /top/bfm2/RW
add wave -noupdate /top/bfm2/BURST
add wave -noupdate -radix decimal /top/bfm2/ADDR
add wave -noupdate -radix decimal /top/bfm2/DATAI
add wave -noupdate -radix decimal /top/bfm2/DATAO
add wave -noupdate -divider {BFM 3}
add wave -noupdate /top/bfm3/CLK
add wave -noupdate /top/bfm3/RST
add wave -noupdate /top/bfm3/VALID
add wave -noupdate /top/bfm3/READY
add wave -noupdate /top/bfm3/RW
add wave -noupdate /top/bfm3/BURST
add wave -noupdate -radix decimal /top/bfm3/ADDR
add wave -noupdate -radix decimal /top/bfm3/DATAI
add wave -noupdate -radix decimal /top/bfm3/DATAO
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {212 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {165 ns} {813 ns}
