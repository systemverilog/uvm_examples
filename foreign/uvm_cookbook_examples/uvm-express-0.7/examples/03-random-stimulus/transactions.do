onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -subitemconfig {/uvm_root/uvm_test_top/agent1/driver/stream.s0.s0 -expand} /uvm_root/uvm_test_top/agent1/driver/stream
add wave -noupdate -expand -subitemconfig {/uvm_root/uvm_test_top/agent2/driver/stream.s0.s0 -expand} /uvm_root/uvm_test_top/agent2/driver/stream
add wave -noupdate -expand -subitemconfig {/uvm_root/uvm_test_top/agent3/driver/stream.s0.s0 -expand} /uvm_root/uvm_test_top/agent3/driver/stream
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {34324 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {10856 ns} {11367 ns}
