vlib work
vlog  -cover f -fsmverbose ../dut/rtl.sv
vlog +define+OVL_SVA_INTERFACE +incdir+../../ovl-2.6 -sv ../../ovl-2.6/ovl_handshake.v
vlog  \
      +incdir+../abc_pkg \
      +incdir+../xyz_pkg \
      ../abc_pkg/abc_pkg.sv \
      ../xyz_pkg/xyz_pkg.sv \
      +incdir+../axx_pkg \
      ../axx_pkg/axx_pkg.sv
vlog  \
      top.sv \
      ../abc_bfm/abc_if.svh \
      ../xyz_bfm/xyz_if.svh \
      tests.sv
vsim -c top tests
run -all
