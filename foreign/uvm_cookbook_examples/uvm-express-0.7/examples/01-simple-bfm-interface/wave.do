onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {BFM 1}
add wave -noupdate /top/bfm/RST
add wave -noupdate /top/bfm/VALID
add wave -noupdate /top/bfm/READY
add wave -noupdate /top/bfm/RW
add wave -noupdate /top/bfm/ADDR
add wave -noupdate /top/bfm/DATAI
add wave -noupdate /top/bfm/DATAO
add wave -noupdate /top/bfm/fire
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {949 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1 us}
