//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------




module tests();















// Tests start here.
  initial begin
    test1(); // Run a test.
    test2(); // Another test. Burst.
    $display("UVM TEST PASSED");
    $finish(2);


  end

  task test1();
    int rdata, wdata;

    $display("\nTest 1 - reset, write/read");

    top.bfm.reset();

    for(int addr = 40; addr < 50; addr++) begin

      wdata = addr;
      top.bfm.write(addr, wdata); // Write
      top.bfm.read( addr, rdata); // Read

      if ( wdata != rdata )
        $display("ERROR: Data mistach @%0x (expected %0x, got %0x)", 
          addr, wdata, rdata);
    end
    top.dut.dump();
  endtask

  task test2();
    static bit [31:0]wdata_array[4] = '{'h04, 'h03, 'h02, 'h01};
    static bit [31:0]xdata_array[4] = '{'hf4, 'hf3, 'hf2, 'hf1};
    bit [31:0]rdata_array[4];
      
    $display("\nTest 2 - reset, burst_write/burst_read");

    top.bfm.reset();

    for (int start_addr = 'h10; 
             start_addr <= 'h70; 
             start_addr += 'h10) begin

      top.bfm.burst_write(start_addr,   wdata_array);
      top.bfm.burst_write(start_addr+8, xdata_array);

      rdata_array = '{0, 0, 0, 0}; 
      top.bfm.burst_read( start_addr, rdata_array);

      foreach(wdata_array[i])
        if ( wdata_array[i] != rdata_array[i] )
$display("ERROR: Data mistach @%0x, element %0x (expected %0x, got %0x)", 
        start_addr+i, i, wdata_array[i], rdata_array[i]); 
    end
    top.dut.dump();
  endtask
endmodule
