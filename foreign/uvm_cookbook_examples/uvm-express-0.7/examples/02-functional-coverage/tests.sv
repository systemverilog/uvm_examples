//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

module tests();
  import uvm_pkg::*; // Bring in run_test().

  import axx_coverage_pkg::*; //  Bring in the AXX Coverage VIP.

  initial begin
    // Put the BFM handles into the ENV. The ENV is not yet built,
    // so the BFM "handles" are stored in a configuration database.

    // "Set" by type, using register_*(name, vif)
    axx_coverage_test::register_abc("bfm1", top.bfm1);
    axx_coverage_test::register_xyz("bfm2", top.bfm2);
    axx_coverage_test::register_xyz("bfm3", top.bfm3);

    uvm_config_db#(int)::set(uvm_root::get(), "*", "recording_detail", 1);
    run_test("axx_coverage_test");
  end



// Tests start here.
  initial begin
    test1(); // Run a test.
    test2(); // Another test. Burst.
    $display("UVM TEST PASSED");
    //$finish(2);
    // Cause UVM phasing to complete.
    axx_coverage_test::ready_to_finish();  
  end

  task test1();
  endtask

  task test2();
    fork
      begin
        test1_bfm1(); // Run a test.
        test2_bfm1(); // Another test. Burst.
      end
      begin
        test1_bfm2(); // Run a test.
        test2_bfm2(); // Another test. Burst.
      end
      begin
        test1_bfm3(); // Run a test.
        test2_bfm3(); // Another test. Burst.
      end
    join
  endtask

  task test1_bfm1();
    $display("\nTest 1 - reset, write/read");
    top.bfm1.reset();
    for(int addr = 40; addr < 50; addr++) begin
      int rdata;
      top.bfm1.write(addr, addr);
      top.bfm1.read(addr, rdata);
      if ( addr != rdata )
$display("ERROR: Data mistach @ ADDR %0x (%0x != %0x)", 
        addr, addr, rdata);
    end
    top.dut.dump();
  endtask

  task test2_bfm1();
    automatic bit [31:0]wdata_array[4] = '{'h04, 'h03, 'h02, 'h01};
    automatic bit [31:0]xdata_array[4] = '{'hf4, 'hf3, 'hf2, 'hf1};
    automatic bit [31:0]rdata_array[4];
      
    $display("\nTest 2 - reset, burst_write/burst_read");
    top.bfm1.reset();

    for (int start_addr = 'h10; 
             start_addr <= 'h70; 
             start_addr += 'h10) begin

      top.bfm1.burst_write(start_addr,   wdata_array);
      top.bfm1.burst_write(start_addr+8, xdata_array);
      rdata_array = '{0, 0, 0, 0}; 
      top.bfm1.burst_read( start_addr, rdata_array);
      foreach(wdata_array[i])
        if ( wdata_array[i] != rdata_array[i] )
$display("ERROR: Data mistach @ addr %0x, element %0x (expected %0x, got %0x)", 
        start_addr+i, i, wdata_array[i], rdata_array[i]); 
    end
    top.dut.dump();
  endtask

  task test1_bfm2();
    $display("\nTest 1 - reset, write/read");
    top.bfm2.reset();
    for(int addr = 40; addr < 50; addr++) begin
      int rdata;
      top.bfm2.write(addr, addr);
      top.bfm2.read(addr, rdata);
      if ( addr != rdata )
$display("ERROR: Data mistach @ ADDR %0x (%0x != %0x)", 
        addr, addr, rdata);
    end
    top.dut.dump();
  endtask

  task test2_bfm2();
    automatic bit [31:0]wdata_array[4] = '{'h04, 'h03, 'h02, 'h01};
    automatic bit [31:0]xdata_array[4] = '{'hf4, 'hf3, 'hf2, 'hf1};
    automatic bit [31:0]rdata_array[4];
      
    $display("\nTest 2 - reset, burst_write/burst_read");
    top.bfm2.reset();

    for (int start_addr = 'h10; 
             start_addr <= 'h70; 
             start_addr += 'h10) begin

      top.bfm2.burst_write(start_addr,   wdata_array);
      top.bfm2.burst_write(start_addr+8, xdata_array);
      rdata_array = '{0, 0, 0, 0}; 
      top.bfm2.burst_read( start_addr, rdata_array);
      foreach(wdata_array[i])
        if ( wdata_array[i] != rdata_array[i] )
$display("ERROR: Data mistach @ addr %0x, element %0x (expected %0x, got %0x)", 
        start_addr+i, i, wdata_array[i], rdata_array[i]); 
    end
    top.dut.dump();
  endtask
  task test1_bfm3();
    $display("\nTest 1 - reset, write/read");
    top.bfm3.reset();
    for(int addr = 40; addr < 50; addr++) begin
      int rdata;
      top.bfm3.write(addr, addr);
      top.bfm3.read(addr, rdata);
      if ( addr != rdata )
$display("ERROR: Data mistach @ ADDR %0x (%0x != %0x)", 
        addr, addr, rdata);
    end
    top.dut.dump();
  endtask

  task test2_bfm3();
    automatic bit [31:0]wdata_array[4] = '{'h04, 'h03, 'h02, 'h01};
    automatic bit [31:0]xdata_array[4] = '{'hf4, 'hf3, 'hf2, 'hf1};
    automatic bit [31:0]rdata_array[4];
      
    $display("\nTest 2 - reset, burst_write/burst_read");
    top.bfm3.reset();

    for (int start_addr = 'h10; 
             start_addr <= 'h70; 
             start_addr += 'h10) begin

      top.bfm3.burst_write(start_addr,   wdata_array);
      top.bfm3.burst_write(start_addr+8, xdata_array);
      rdata_array = '{0, 0, 0, 0}; 
      top.bfm3.burst_read( start_addr, rdata_array);
      foreach(wdata_array[i])
        if ( wdata_array[i] != rdata_array[i] )
$display("ERROR: Data mistach @ addr %0x, element %0x (expected %0x, got %0x)", 
        start_addr+i, i, wdata_array[i], rdata_array[i]); 
    end
    top.dut.dump();
  endtask
endmodule
