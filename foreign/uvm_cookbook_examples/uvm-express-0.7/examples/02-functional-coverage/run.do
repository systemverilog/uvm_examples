vlib work
vlog  -cover f -fsmverbose ../dut/rtl.sv
vlog +define+OVL_SVA_INTERFACE +incdir+../../ovl-2.6 -sv ../../ovl-2.6/ovl_handshake.v
# VIP Compilation
vlog  \
      +incdir+../abc_coverage_pkg \
      +incdir+../xyz_coverage_pkg \
        ../abc_coverage_pkg/abc_coverage_pkg.sv \
        ../xyz_coverage_pkg/xyz_coverage_pkg.sv \
      +incdir+../axx_coverage_pkg \
        ../axx_coverage_pkg/axx_coverage_pkg.sv
vlog  \
      top.sv \
      ../abc_bfm/abc_if.svh \
      ../xyz_bfm/xyz_if.svh \
      tests.sv
vsim -c top tests
run -all
