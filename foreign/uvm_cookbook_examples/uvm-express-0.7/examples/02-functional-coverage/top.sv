//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

module top();
  reg clk;

  abc_if bfm1(clk); // BFM - the Bus Functional Model
  xyz_if bfm2(clk); // BFM 2
  xyz_if bfm3(clk); // BFM 3

  dut_mem dut(      // DUT
    .CLK1(clk), 
    .RST1(bfm1.RST), 
    .RW1(bfm1.RW), 
    .READY1(bfm1.READY), 
    .VALID1(bfm1.VALID), 
    .ADDR1(bfm1.ADDR), 
    .DATAI1(bfm1.DATAI),
    .DATAO1(bfm1.DATAO),

    .CLK2(clk), 
    .RST2(bfm2.RST), 
    .RW2(bfm2.RW), 
    .READY2(bfm2.READY), 
    .VALID2(bfm2.VALID), 
    .ADDR2(bfm2.ADDR), 
    .DATAI2(bfm2.DATAI),
    .DATAO2(bfm2.DATAO),

    .CLK3(clk), 
    .RST3(bfm3.RST), 
    .RW3(bfm3.RW), 
    .READY3(bfm3.READY), 
    .VALID3(bfm3.VALID), 
    .ADDR3(bfm3.ADDR), 
    .DATAI3(bfm3.DATAI),
    .DATAO3(bfm3.DATAO)
  );

  // Turn on the clock.
  always begin
    #10 clk <= 1;
    #10 clk <= 0; 
  end
endmodule
