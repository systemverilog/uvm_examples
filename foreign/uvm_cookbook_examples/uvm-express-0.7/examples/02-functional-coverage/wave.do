onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {BFM 1}
add wave -noupdate /top/bfm/CLK
add wave -noupdate /top/bfm/RST
add wave -noupdate /top/bfm/VALID
add wave -noupdate /top/bfm/READY
add wave -noupdate /top/bfm/RW
add wave -noupdate /top/bfm/BURST
add wave -noupdate -radix decimal /top/bfm/ADDR
add wave -noupdate -radix decimal /top/bfm/DATAI
add wave -noupdate -radix decimal /top/bfm/DATAO
add wave -noupdate -divider {BFM 2}
add wave -noupdate /top/bfm2/CLK
add wave -noupdate /top/bfm2/RST
add wave -noupdate /top/bfm2/VALID
add wave -noupdate /top/bfm2/READY
add wave -noupdate /top/bfm2/RW
add wave -noupdate /top/bfm2/BURST
add wave -noupdate -radix decimal /top/bfm2/ADDR
add wave -noupdate -radix decimal /top/bfm2/DATAI
add wave -noupdate -radix decimal /top/bfm2/DATAO
add wave -noupdate -divider {BFM 3}
add wave -noupdate /top/bfm3/CLK
add wave -noupdate /top/bfm3/RST
add wave -noupdate /top/bfm3/VALID
add wave -noupdate /top/bfm3/READY
add wave -noupdate /top/bfm3/RW
add wave -noupdate /top/bfm3/BURST
add wave -noupdate -radix decimal /top/bfm3/ADDR
add wave -noupdate -radix decimal /top/bfm3/DATAI
add wave -noupdate -radix decimal /top/bfm3/DATAO
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {212 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {2478 ns}
