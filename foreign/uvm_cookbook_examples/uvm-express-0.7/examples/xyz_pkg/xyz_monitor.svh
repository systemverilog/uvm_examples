//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class xyz_monitor extends uvm_monitor;
  `uvm_component_utils(xyz_monitor)

  virtual xyz_if bfm;
  uvm_analysis_port #(xyz_item) ap;

  function new(string name = "xyz_monitor", 
      uvm_component parent = null);
    super.new(name, parent);
  endfunction

  function void build_phase(uvm_phase phase);
    ap = new("ap", this);
  endfunction

  task run_phase(uvm_phase phase);
    xyz_item t;

    if (bfm == null) begin
        `uvm_fatal("MONITOR", "No BFM or virtual interface")
    end

    bfm.monitor_stream_h = $create_transaction_stream(
      {"..", get_full_name(), ".monitor_stream"}, "kind");

    t = xyz_item::type_id::create("t");
    forever begin
      bit transaction_ok;

      // Each call to monitor waits for at least one clock
      bfm.monitor(transaction_ok, 
        t.rw, t.addr, t.data, t.burst);
      if (transaction_ok) begin
        `uvm_info("MONITOR", 
          $sformatf(" %s (%s)", 
          (t.burst==1)?" Burst":"Normal", t.convert2string()),
          UVM_MEDIUM)
        ap.write(t);
        t = xyz_item::type_id::create("t");
      end
    end
  endtask
endclass
