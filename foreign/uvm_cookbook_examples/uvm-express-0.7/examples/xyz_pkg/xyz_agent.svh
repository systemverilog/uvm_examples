//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class xyz_agent extends uvm_agent;
  `uvm_component_utils(xyz_agent)

  // Set from configurations
  virtual xyz_if bfm;

  xyz_monitor monitor;
  xyz_coverage coverage; // TODO: multiple covergroups

  xyz_sequencer sequencer; // active
  xyz_driver driver;       // active

  function new(string name = "xyz_agent", 
      uvm_component parent = null);
    super.new(name, parent);
    uvm_config_db#(int)::set(this, "*", "recording_detail", 1);
  endfunction

  function void build_phase(uvm_phase phase);
    if( !uvm_config_db#(virtual xyz_if)::get( this, "", 
        "bfm", bfm)) begin
      `uvm_fatal("ENV", "BFM not set")
    end
    driver   =   xyz_driver::type_id::create("driver",  this);
    monitor  =  xyz_monitor::type_id::create("monitor", this);
    coverage = xyz_coverage::type_id::create("coverage",this);
    sequencer=xyz_sequencer::type_id::create("sequencer",this);
  endfunction

  function void connect_phase(uvm_phase phase);
    monitor.bfm = bfm;
    monitor.ap.connect(coverage.analysis_export);
    driver.bfm = bfm;
    driver.seq_item_port.connect(sequencer.seq_item_export);
  endfunction

  task run_phase(uvm_phase phase);
    `uvm_info("RUN", $sformatf("Starting up '%s'", 
      get_full_name()), UVM_MEDIUM)
    // bfm.dump();
  endtask

  function void check_phase(uvm_phase phase);
  endfunction
endclass

