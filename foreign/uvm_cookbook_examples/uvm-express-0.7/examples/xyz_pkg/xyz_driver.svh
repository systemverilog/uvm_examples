//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class xyz_driver extends uvm_driver #(xyz_item);
  `uvm_component_utils(xyz_driver)

  virtual xyz_if bfm;

  function new(string name = "xyz_driver", 
      uvm_component parent = null);
    super.new(name, parent);
  endfunction

  task run_phase(uvm_phase phase);
    xyz_item item;
    xyz_reset_item reset_item;

    bfm.stream_h = $create_transaction_stream(
      {"..", get_full_name(), ".stream"}, "kind");

    forever begin
      seq_item_port.get_next_item(item);
      `uvm_info("DRIVER", 
        $sformatf("item=%s", item.convert2string()), 
        UVM_MEDIUM)

      if ($cast(reset_item, item))
        bfm.reset();
      else if (item.rw == 1) // READ
        if (item.burst == 1)
          bfm.burst_read(item.addr, item.data);
        else
          bfm.read(item.addr, item.data[0]);
      else
        if (item.burst == 1)
          bfm.burst_write(item.addr, item.data);
        else
          bfm.write(item.addr, item.data[0]);

      seq_item_port.item_done();
    end
  endtask
endclass
