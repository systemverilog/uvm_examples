//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class abc_sequence extends uvm_sequence#(abc_item);
  `uvm_object_utils(abc_sequence)

  int number_of_items = 0; // 0 means do forever

  rand bit[31:0] addr;

  rand bit[31:0] addr_low;
  rand bit[31:0] addr_high;

  constraint rand_range {
    addr_low >= 0;
    addr_high <  'h80;
    addr_low < addr_high;
  }

  constraint addr_value {
  }

  function new(string name = "abc_sequence");
    super.new(name);
    addr_low = 'h00;
    addr_high = 'h80;
    addr_value.constraint_mode(0);
  endfunction

  task body();
    int do_forever;
    abc_item item;
    abc_reset_item reset_item;

    `uvm_info("SEQUENCE", "body() running...", UVM_MEDIUM)

    // Reset the hardware
    reset_item = abc_reset_item::type_id::create("reset_item");
    start_item(reset_item);
    finish_item(reset_item);

    if (number_of_items == 0)
      do_forever = 1;
    else
      do_forever = 0;

    // Crank out reads and writes forever.
    while(1) begin
      if (!do_forever) begin
        number_of_items--;
        if ( number_of_items <= 0)
          break;
      end
      item = abc_item::type_id::create("item");

      start_item(item);
      if (!item.randomize() with { addr >= local::addr_low; 
                                   addr <  local::addr_high; })
      begin 
        `uvm_fatal("SEQUENCE", "Randomize FAILED")
      end
      finish_item(item);
    end
    `uvm_info("SEQUENCE", "body() done...", UVM_MEDIUM)
  endtask
endclass



class abc_even_sequence extends abc_sequence;
  `uvm_object_utils(abc_even_sequence)

  constraint addr_value {
    addr == 1'h0;
  }

  function new(string name = "abc_even_sequence");
    super.new(name);
    addr_value.constraint_mode(1);
  endfunction

  task body();
    `uvm_info("SEQUENCE EVEN", "body() running...", UVM_MEDIUM)
    super.body();
    `uvm_info("SEQUENCE EVEN", "body() done...", UVM_MEDIUM)
  endtask
endclass


//TODO: Write a sequence which writes the entire ROW. Using bursts 
//TODO:  or single writes.
//TODO: Same for columns.
//TODO: Checkerboard.
//TODO: Characters!?
//TODO: Any gif?

