//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class abc_item extends uvm_sequence_item;
  `uvm_object_utils(abc_item)

  rand bit burst;
  rand bit rw;
  rand bit[31:0] addr;
  rand bit[31:0] data[4];

  constraint val_addr { 
    addr[1:0] == 'b00;   // Any address divisible by 4.
    addr > 0;            // ...between 0
    addr < 127;          // ... and 127
  }

  constraint val_data { 
    // TODO: Write a better constraint...
    data[0] > 0; data[0] < 127; 
    data[1] > 0; data[1] < 127; 
    data[2] > 0; data[2] < 127; 
    data[3] > 0; data[3] < 127; 
  }

  function new(string name = "abc_item");
    super.new(name);
  endfunction

  typedef enum bit[1:0] {READ, WRITE, BURST_READ, BURST_WRITE} tr_type_t;
  
  function tr_type_t get_tr_type();
    tr_type_t tr_type;

         if ((rw == 1) && (burst == 1)) tr_type = BURST_READ;
    else if ((rw == 1) && (burst == 0)) tr_type = READ;
    else if ((rw == 0) && (burst == 1)) tr_type = BURST_WRITE;
    else if ((rw == 0) && (burst == 0)) tr_type = WRITE;
    return tr_type;
  endfunction

  function void post_randomize();
    // TODO: Write a better constraint...
    // Easier to debug with in-order data.
    data[0] = addr;
    if (burst) begin
      data[1] = data[0] + 1;
      data[2] = data[1] + 1;
      data[3] = data[2] + 1;
    end
    else begin 
      data[0] |= 'h0000_0100;
      data[1] = 0; 
      data[2] = 0; 
      data[3] = 0;
    end
  endfunction

  function string convert2string();
    string data_string;

    // Format the data. 4 words for burst, one for non-burst.
    if (burst == 1) 
      data_string = $sformatf("%0x %0x %0x %0x", 
        data[0], data[1], data[2], data[3]);
    else             
      data_string = $sformatf("%0x", data[0]);

    // Return the formatted "value" of this transaction.
    return $sformatf("%s(%0x, %s)", get_tr_type(), addr, data_string);
  endfunction

  function void do_record(uvm_recorder recorder);
    if (recorder == null)
      return;
    if (recorder.tr_handle == 0)
      return;
    if (!is_recording_enabled())
      return;

    // Record an enum 'tr_type', so that a nice transaction 
    // recording output can be viewed.
    `uvm_record_attribute(recorder.tr_handle, "type", get_tr_type());
    `uvm_record_attribute(recorder.tr_handle, "addr", addr);
    `uvm_record_attribute(recorder.tr_handle, "data", data);
  endfunction
endclass


class abc_reset_item extends abc_item;
  `uvm_object_utils(abc_reset_item)

  function new(string name = "abc_reset_item");
    super.new(name);
  endfunction

  function string convert2string();
    return "RESET";
  endfunction

  function void do_record(uvm_recorder recorder);
    if (recorder == null)
      return;
    if (recorder.tr_handle == 0)
      return;
    if (!is_recording_enabled())
      return;
    `uvm_record_attribute(recorder.tr_handle, "reset", 1);
  endfunction
endclass
