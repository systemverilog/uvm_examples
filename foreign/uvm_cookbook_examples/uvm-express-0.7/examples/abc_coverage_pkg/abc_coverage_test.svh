//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class abc_coverage_test extends uvm_env;
  `uvm_component_utils(abc_coverage_test)

  // Set from configurations
  virtual abc_if bfm1;
  virtual abc_if bfm2;
  virtual abc_if bfm3;

  abc_coverage_agent agent1;
  abc_coverage_agent agent2;
  abc_coverage_agent agent3;

  function new(string name = "abc_coverage_test", 
      uvm_component parent = null);
    super.new(name, parent);
  endfunction

  static function void register(string name, 
      virtual abc_if vif);
    uvm_config_db#(virtual abc_if)::set(
      uvm_root::get(), "", name, vif);
  endfunction

  function void build_phase(uvm_phase phase);
    if( !uvm_config_db#(virtual abc_if)::get(
        this, "", "bfm1", bfm1)) begin
      `uvm_fatal("ENV", "BFM1 not set")
    end
    uvm_config_db#(virtual abc_if)::set(
        this, "agent1", "bfm", bfm1);

    if( !uvm_config_db#(virtual abc_if)::get(
        this, "", "bfm2", bfm2)) begin
      `uvm_fatal("ENV", "BFM2 not set")
    end
    uvm_config_db#(virtual abc_if)::set(
        this, "agent2", "bfm", bfm2);

    if( !uvm_config_db#(virtual abc_if)::get(
        this, "", "bfm3", bfm3)) begin
      `uvm_fatal("ENV", "BFM3 not set")
    end
    uvm_config_db#(virtual abc_if)::set(
        this, "agent3", "bfm", bfm3);

    agent1 = abc_coverage_agent::type_id::create("agent1", this);
    agent2 = abc_coverage_agent::type_id::create("agent2", this);
    agent3 = abc_coverage_agent::type_id::create("agent3", this);
  endfunction

  task run_phase(uvm_phase phase);
    `uvm_info("RUN", $sformatf("Starting up '%s'", 
      get_full_name()), UVM_MEDIUM)

    phase.raise_objection(this);
  endtask

  static task ready_to_finish();
    uvm_phase run_phase;
    abc_coverage_agent::ready_to_finish();
    run_phase = uvm_run_phase::get();
    run_phase.clear();
  endtask
endclass
