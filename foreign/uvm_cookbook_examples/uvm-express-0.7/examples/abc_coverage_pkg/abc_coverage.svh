//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class abc_coverage extends uvm_subscriber#(abc_item);
  `uvm_component_utils(abc_coverage)

  abc_item t;

  covergroup cg;
    burst_cp: coverpoint t.burst;
    rw_cp:    coverpoint t.rw;
    addr_cp:  coverpoint t.addr {
      bins regular[] = { [0:99] };
      ignore_bins  ignore_addr = { [100:$] };
    }
    data0_cp:  coverpoint t.data[0] {
      bins regular[] = { [0:99] };
      ignore_bins  ignore_data = { [100:$] };
    }
    data1_cp:  coverpoint t.data[1] iff (t.burst) {
      bins regular[] = { [0:99] };
      ignore_bins  ignore_data = { [100:$] };
    }
    data2_cp:  coverpoint t.data[2] iff (t.burst) {
      bins regular[] = { [0:99] };
      ignore_bins  ignore_data = { [100:$] };
    }
    data3_cp:  coverpoint t.data[3] iff (t.burst) {
      bins regular[] = { [0:99] };
      ignore_bins  ignore_data = { [100:$] };
    }

    burst_X_rw: cross burst_cp, rw_cp;
     addr_X_rw: cross  addr_cp, rw_cp;
    data0_X_rw: cross data0_cp, rw_cp;
  endgroup

  virtual function void sample(abc_item t);
    this.t = t;
    cg.sample();
  endfunction

  function new(string name = "abc_coverage", 
      uvm_component parent = null);
    super.new(name, parent);
    cg = new();
  endfunction

  function void write(abc_item t);
    sample(t);
    `uvm_info("COVERAGE", $sformatf("Coverage=%0d%% (t=%s)", 
      cg.get_inst_coverage(), t.convert2string()), UVM_MEDIUM)
  endfunction
endclass

/* Note about using 'covergroup cg with function sample(tr t)
   (We can't use a class handle)

19.8.1 Overriding the built-in sample method 
  ...
  The formal arguments of an overridden sample method shall be
  searched before the enclosing scope; each such argument may
  only designate a coverpoint or conditional guard expression.
  It shall be an error to use a sample formal argument in any 
  context other than a coverpoint or conditional guard 
  expression. Formal sample arguments shall not designate an 
  output direction. The formal arguments of an overridden 
  sample method belong to the same lexical scope as the formal
  arguments to the covergroup (consumed by the covergroup new 
  operator). Hence, it shall be an error for the same argument
  name to be specified in both argument lists. 
*/

