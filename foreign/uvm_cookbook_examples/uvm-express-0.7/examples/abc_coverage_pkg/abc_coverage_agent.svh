//------------------------------------------------------------
//   Copyright 2012 Mentor Graphics Corporation
//   All Rights Reserved Worldwide
//   
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//   
//       http://www.apache.org/licenses/LICENSE-2.0
//   
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
//------------------------------------------------------------

class abc_coverage_agent extends uvm_agent;
  `uvm_component_utils(abc_coverage_agent)

  // Set from configurations
  virtual abc_if bfm;

  abc_monitor monitor;
  abc_coverage coverage;

  function new(string name = "abc_coverage_agent", 
      uvm_component parent = null);
    super.new(name, parent);
    uvm_config_db#(int)::set(this, "*", "recording_detail", 0);
  endfunction

  function void build_phase(uvm_phase phase);
    if( !uvm_config_db#(virtual abc_if)::get(
        this, "", "bfm", bfm)) begin
      `uvm_fatal("ENV", "BFM not set")
    end
    monitor  =  abc_monitor::type_id::create("monitor",  this);
    coverage = abc_coverage::type_id::create("coverage", this);
  endfunction

  function void connect_phase(uvm_phase phase);
    monitor.bfm = bfm;
    monitor.ap.connect(coverage.analysis_export);
  endfunction

  task run_phase(uvm_phase phase);
    `uvm_info("RUN", $sformatf("Starting up '%s'", 
      get_full_name()), UVM_MEDIUM)

    phase.raise_objection(this);
    bfm.dump();
  endtask

  static task ready_to_finish();
    uvm_phase run_phase;
    run_phase = uvm_run_phase::get();
    run_phase.clear();
  endtask

  function void check_phase(uvm_phase phase);
  endfunction
endclass

